<?php
declare(strict_types=1);

namespace Idlemon\IdlemonPhp\Tests;

use Idlemon\IdlemonPhp\Idlemon;
use PHPUnit\Framework\TestCase;

/**
 * Class BoxcarTest
 *
 * @package            Idlemon\IdlemonPhp\Tests
 *
 * @coversDefaultClass Idlemon\IdlemonPhp\Idlemon
 */
class IdlemonTest extends TestCase
{

    /**
     * @covers ::__construct
     */
    public function testConstruct()
    {
        $idlemon = new Idlemon('secret_project_token');

        try {
            $reflector = new \ReflectionClass($idlemon);

            $projectToken = $reflector->getProperty('projectToken');
            $projectToken->setAccessible(true);
            self::assertSame('secret_project_token', $projectToken->getValue($idlemon));
        } catch (\ReflectionException $e) {
            self::fail($e);
        }
    }
}
