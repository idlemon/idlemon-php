<?php
declare(strict_types=1);

namespace Idlemon\IdlemonPhp\Tests;

use Idlemon\IdlemonPhp\Log;
use PHPUnit\Framework\TestCase;

/**
 * Class BoxcarTest
 *
 * @package            Idlemon\IdlemonPhp\Tests
 *
 * @coversDefaultClass Idlemon\IdlemonPhp\Log
 */
class LogTest extends TestCase
{

    /**
     * @covers ::__construct
     */
    public function testSetMonitorSlug()
    {
        $idlemon = new Log('foo');

        try {
            $reflector = new \ReflectionClass($idlemon);

            $monitorSlug = $reflector->getProperty('monitorSlug');
            $monitorSlug->setAccessible(true);
            self::assertSame('foo', $monitorSlug->getValue($idlemon));
        } catch (\ReflectionException $e) {
            self::fail($e);
        }
    }
}
