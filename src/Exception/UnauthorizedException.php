<?php
declare(strict_types=1);

namespace Idlemon\IdlemonPhp\Exception;

/**
 * Class UnauthorizedException
 *
 * @package Idlemon\IdlemonPhp
 */
class UnauthorizedException extends \Exception
{
}
