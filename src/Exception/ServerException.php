<?php
declare(strict_types=1);

namespace Idlemon\IdlemonPhp\Exception;

/**
 * Class ServerException
 *
 * @package Idlemon\IdlemonPhp
 */
class ServerException extends \Exception
{
}
