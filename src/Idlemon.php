<?php
declare(strict_types=1);

namespace Idlemon\IdlemonPhp;

use Idlemon\IdlemonPhp\Exception\ServerException;
use Idlemon\IdlemonPhp\Exception\UnauthorizedException;

/**
 * Class Idlemon
 *
 * @package Idlemon\IdlemonPhp
 */
class Idlemon
{

    const ENDPOINT = 'https://www.idlemon.net/api/monitors/';

    /**
     * @var string Project token
     */
    private $projectToken;

    /**
     * The token can be found on the project page
     *
     * @param string $projectToken Project token
     */
    public function __construct($projectToken)
    {
        $this->projectToken = $projectToken;
    }

    /**
     * Create log object for a monitor identified by slug
     *
     * @param string $monitorSlug Monitor slug
     *
     * @return Log Log object
     */
    public static function createLog(string $monitorSlug): Log
    {
        return new Log($monitorSlug);
    }

    /**
     * Send log to Idlemon
     *
     * @param Log $log Log object
     *
     * @return bool TRUE if successful, otherwise FALSE
     *
     * @throws ServerException If server exception occurs while sending log to Idlemon
     * @throws UnauthorizedException If authentication fails
     */
    public function send(Log $log): bool
    {
        /**
         * Init curl, execute and close
         */
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, self::ENDPOINT . $log->getMonitorSlug() . '/logs');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->createJsonPayload($log));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Accept: application/json',
            'Content-Type: application/json',
            "Api-Token: $this->projectToken"
        ]);

        curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        /**
         * Handle result
         */
        switch ($info['http_code']) {
            case 201:
                return true;

            case 401:
                throw new UnauthorizedException('Unauthorized request: Project token not recognized');

            case 500:
                throw new ServerException('Internal server error: Unknown error');
        }

        return false;
    }

    /**
     * Create JSON payload for API request
     *
     * @param Log $log
     *
     * @return string JSON payload for post fields
     */
    protected function createJsonPayload(Log $log)
    {
        $data = [
            'duration' => $log->getDuration(),
            'metas'    => $log->getMetas()
        ];

        return json_encode($data);
    }
}
