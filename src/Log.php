<?php
declare(strict_types=1);

namespace Idlemon\IdlemonPhp;

/**
 * Class Log
 *
 * @package Idlemon\IdlemonPhp
 */
class Log
{

    /**
     * @var string Monitor slug
     */
    protected $monitorSlug;

    /**
     * @var array Metas
     */
    protected $metas = [];

    /**
     * @var float Start time
     */
    protected $timeStart;

    /**
     * @param string $monitorSlug Monitor slug
     */
    public function __construct(string $monitorSlug)
    {
        $this->monitorSlug = $monitorSlug;
        $this->timeStart = microtime(true);
    }

    /**
     * Set monitor slug
     *
     * @param string $monitorSlug Monitor slug
     */
    public function setMonitorSlug(string $monitorSlug)
    {
        $this->monitorSlug = $monitorSlug;
    }

    /**
     * Get monitor slug
     *
     * @return string Monitor slug
     */
    public function getMonitorSlug(): string
    {
        return $this->monitorSlug;
    }

    /**
     * Set meta with name and value
     *
     * @param string $metaName
     * @param $metaValue
     */
    public function setMeta(string $metaName, $metaValue)
    {
        $this->metas[] = [
            'name'  => $metaName,
            'value' => $metaValue
        ];
    }

    /**
     * Get metas array
     *
     * @return array
     */
    public function getMetas(): array
    {
        return $this->metas;
    }

    /**
     * Get duration since start
     *
     * @return float Time in seconds
     */
    public function getDuration(): float
    {
        return microtime(true) - $this->timeStart;
    }
}
